//Aller chercher le tableau person
let personTab;
let personContainer = fetch('../data/person.json')
.then(response => response.json())
.then(data => {
    personTab = data;
    renderTable1(personTab);
});
//Afficher le tableau person
function renderTable1(data) {
    let html = '<table><tr><th>Id</th><th>Nom</th><th>Genre</th><th>Age</th></tr>';
    for (let person of data) {
        html+= '<tr>';
        html += `<td>${person.id}</td> <td contenteditable="true">${person.name}</td><td contenteditable="true"> ${person.gender}<td contenteditable="true"> ${person.age}</td>`;
        html+= '</tr>';
    }
    html += '</table>';
    document.querySelector('#personContainer').innerHTML = html;  
};
//Aller chercher le tableau points
let pointsTab;
let pointsContainer = fetch('../data/points.json')
.then(response => response.json())
.then(data => {
    pointsTab = data;
    renderTable2(pointsTab);
});
//Afficher le tableau points
function renderTable2(data) {
    let html = '<table><tr><th>Id</th><th>Nom</th><th>Genre</th><th>Age</th></tr>';
    for (let points of data) {
        html+= '<tr>';
        html += `<td>${points.id}</td> <td contenteditable="true">${points.x}</td><td contenteditable="true"> ${points.y}<td contenteditable="true"> ${points.z}</td>`;
        html+= '</tr>';
    }
    html += '</table>';
    document.querySelector('#pointsContainer').innerHTML = html;  
};
//Aller chercher le tableau product
let productTab;
let productContainer = fetch('../data/product.json')
.then(response => response.json())
.then(data => {
    productTab = data;
    renderTable3(productTab);
});
//Afficher le tableau product
function renderTable3(data) {
    let html = '<table><tr><th>Id</th><th>Nom</th><th>Genre</th><th>Age</th></tr>';
    for (let product of data) {
        html+= '<tr>';
        html += `<td>${product.id}</td> <td contenteditable="true">${product.name}</td><td contenteditable="true"> ${product.price}<td contenteditable="true"> ${product.available}</td><td>${product.tags}</td>`;
        html+= '</tr>';
    }
    html += '</table>';
    document.querySelector('#productContainer').innerHTML = html;  
};
//Filtre tableau personne
function filterDataPerson() {
    let filterValue = document.querySelector('#filterValuePerson').value.toLowerCase();
    let filteredData = personTab.filter(function(person) {
    return (
        person.name.toLowerCase().includes(filterValue) ||
        person.gender === filterValue ||
        person.age == filterValue
    );
    });
    renderTable1(filteredData);
};
//Filtre tableau points
function filterDataPoints() {
    let filterValue = document.querySelector('#filterValuePoints').value.toLowerCase();
    let filteredData = personTab.filter(function(points) {
    return (
        points.x === filterValue ||
        points.y === filterValue ||
        points.z === filterValue
    );
    });
    renderTable2(filteredData);
};
//Filtre tableau product
function filterDataProduct() {
    let filterValue = document.querySelector('#filterValueProduct').value.toLowerCase();
    let filteredData = productTab.filter(function(points) {
    return (
        points.x === filterValue ||
        points.y === filterValue ||
        points.z === filterValue
    );
    });
    renderTable2(filteredData);
};