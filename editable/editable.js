let data
let tableElem 

function sortirData(elem){
    let dataURL = elem.getAttribute('data-source')
    fetch(dataURL)
    .then((response) => response.json())
    .then(
        (lesdatadufichier) => {
            data = lesdatadufichier
            createTable()
        }
    )
}

function createTable(){
    tableElem = document.createElement('table')
    let cont = document.getElementById("productContainer")
    cont.appendChild(tableElem)
    tableHeader()
    tableBody()
}

function tableHeader(){
    console.log(data);
    let unElement = data[0]
    let thead = document.createElement('thead')
    let tr = document.createElement('tr')
    for (let key in unElement){
        let th = document.createElement('th')
        th.innerText = key
        th.addEventListener("click", trierTab)
        tr.appendChild(th)
    }
    thead.appendChild(tr)
    tableElem.appendChild(thead)
}

function tableBody(){
    let tbody = document.createElement('tbody')
    
    for(let obj of data){
        let tr = document.createElement('tr')
        tr.classList.add("ligne")
        for(let key in obj){
            let td = document.createElement('td')
            td.contentEditable = true;
            td.innerText = obj[key]
            tr.appendChild(td)
        }
        tbody.appendChild(tr)
    }
    tableElem.appendChild(tbody)
}

let sortOrder= 1
function trierTab(param){
    let key = param.target.textContent
    sortOrder = sortOrder * -1;
    data.sort(
        (a,b) => (b[key].toString().localeCompare(a[key].toString())) * sortOrder
    )
    tableElem.querySelector('thead')?.remove()
    tableElem.querySelector('tbody')?.remove()
    createTable()
}

function filtrerTab() {
    let filterValue = document.getElementById("filterValueProduct").value.toLowerCase()
    let filterTab = document.querySelectorAll('.ligne')
    console.log(filterTab);
    for(let i of filterTab){
        if(i.innerText.toLowerCase().includes(filterValue.toLowerCase())){
            i.style.display = ""
        }else{
            i.style.display = "none"
        }
    }
}

function ligneADD(){
    
}